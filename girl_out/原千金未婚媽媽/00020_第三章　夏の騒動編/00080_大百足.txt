樹海、距離邊境街道稍遠的西北方。居住在這大自然都市中的精靈族與妖精族、小人族與翼人種等等、自古以來就與冒險者有很深的關係。

因為屬於尚未開拓的地區、魔物更容易出現。它們討伐並且擊退那些接近樹海之都的魔獸、頻繁的驅趕那些魔物、但隨著時間慢慢的也會需要依靠冒險者的協助。

那麼、就遷移吧、居住在森林外的街道或早已開拓的地方不就好了嗎、如此多數的外部意見。但因精靈這種長壽的種族、森林居民特有的觀念、還有活在花草之中的妖精族與小人族、生活在大樹之上翼人族等習性、面對石造的屋子與街道而感到陌生。

因此、隨著氣溫的上升、為了解決大量繁殖的魔物與魔獸而僱用了冒險者。隨著冒險者的出現與啟發、自己也成為了冒險者、橫跨了大陸、揚名立萬。

───

階級最上位的冒險者、Ｓ級冒險者中的翼人種《八咫梟》以及史上首位的小人族冒險者、也是冒險者中唯一的例外、三位一體的Ｓ級冒險者《小鬼眾》

其他凌駕於上位的冒險者也是出自於這片樹海、或許也是樹海之都與冒險者公會關係得深入起到了一定的作用。

「在那裡！是這次的討伐目標！」

在那樣物種豐富的樹海之中。在距離亞人種之城兩里遠的位置上、庫德用右手扔擲出了匕首、匕首上有顆紅色水晶、印著封魔術式的水晶。

「唦唦唦唦唦唦唦唦唦唦！！」

在周圍、那些尚未開墾過、樹海特有的巨大樹木與水晶觸擊的瞬間、啪嘰啪嘰啪嘰！！與尖銳的鳴響一同釋放電擊。

隨著震耳欲聾的悲鳴過後、看見從左右開合的口中吐出酸液與無數的雙腳抽蓄騷動的畫面、拿著弩槍的蕾亞不自覺地起了雞皮疙瘩、大聲靠杯道。

「啊、受夠了！！之前的巨大蜚蠊就算了、不覺得最近一直和在些噁心的東西戰鬥嗎！？」

黑色甲殼的空隙⋯⋯⋯可以看見被貫穿的位置露出微微的嫩肉、上面插著可引爆魔術、盧恩文字的鐵矢。對於正統的精靈來說、手拿的武器可謂邪魔歪道、但她正是擁有一半精靈血脈與天生就具有弓術天賦的種族。

或許是受到了電擊的影響、行動變得遲緩了、不過、蕾亞仍一絲不苟送給眼前正在彎曲與移動的生物三連矢、鐵矢全數命中弱點⋯⋯是她的才能與修練的結果。

「那鐵矢上刻有麻痺的盧恩符文⋯⋯⋯⋯先鋒就拜託了、凱爾！」
「知道了！」

裝備了盾牌與狼牙棒的凱爾衝向那裡。

「《身體・強化》⋯⋯還有、《火炎・衝擊・強化・付加》！！」

強化魔術《體能強化》使全身充滿力量、與平時無法相比的爆發性速度突進、同時詠唱了四節構成的魔術、出現赤紅色的炙熱火焰、像光膜一樣包覆了狼牙棒。

「死吧！！」
「唦唦唦唦唦唦唦唦唦唦唦唦！！」

名為《火焰打擊》的招式直擊魔物的頭。對自身的打擊賦予了火焰與衝擊強化、這是凱爾改變魔術後自創、自身最強大的攻擊。

生物燒焦的味道瞬間刺激凱爾的鼻腔。過往的大熊頭部受到此招攻擊也是瞬間爆炸擊殺、不過、這次的敵人僅造成了甲殼上的一點裂痕。

「好⋯⋯好硬⋯⋯！」
「凱爾退後！」

生物的觸角變成了針刺一樣打算攻擊對象使其中毒、不過比那更快的凱爾早已拉開了可使用魔術的安全距離、兩者之間插入了一到石牆、這是由庫德發動的、從地面產生牆壁的魔術《石牆》來保護同伴。

「那鬼東西絕對不是蜈蚣、認真！剛剛打下去、還以為打到石頭了呢！！」
「再加上那個觸角滴下來的液體⋯⋯地上都冒煙了、那絕對是毒沒錯了」
「而且最初的麻痺也快恢復了⋯⋯那到底是什麼啊、好噁心的蟲」

學名為霹靂大蜈蚣巨型殺手蜈蚣。通稱『大百足』、然而討伐這凶狠醜陋的魔物、便是這次三位Ｅ級冒險者所接受的委託。

全長超過十米的巨型蜈蚣、其兇爆性和凱爾等人所知的蜈蚣是無法比擬的、是一種從家畜到人通吃的食肉蟲。

「想說先下手為強、直接打爆它腦袋的說、沒想到只能造成一點裂痕⋯⋯⋯⋯哭哭了」

再加上覆蓋全身的黑色硬殼相當頑強。很明顯的就是一般人無法對付的、才會委託給冒險者討伐的怪物、但最先要處理的依然是生理上的拒絕與厭惡感。

「噁～哎⋯⋯！快看那個⋯⋯觸角嗶口嗶口的、還有那些腿？啪唦啪唦的⋯⋯⋯⋯好想現在就回家」
「不覺得比之前的巨大蜚蠊好很多嗎⋯⋯⋯⋯？」
「現在是聊天的時候嗎！！！！」

巨大蜈蚣身上數百個黑點所構成的黑色複眼正在直視眼前的三個人。雖然是沒有表情的蟲子、但感覺到現場那無需辯解的殺意與憤怒、大概最初的聯合攻擊惹火它了吧。

「過來了！兩人快散開！！執行計畫Ｂ！！」
「哦哦！！」
「交給我！！」

疾走。突進。用那上百隻腳用衝撞般的速度高速移動、宛如軍馬。衝刺過的泥土留下無數痕跡、折斷了地面上所有的樹枝、三人等待逼近的霹靂大蜈蚣縮短到無法瞬間轉換方向的距離時、橫向跳躍。

「《石壁・展開・三方》！！」
「《水滴・落流》！！」

對這個世上大多數的生物而言、都會朝著臉面向的方向前進、要回頭並不容易。庫德發動了跟剛才同樣的魔術、但根據詠唱的改變《石牆》變成了三管齊下的障礙、阻止對手前進得去路後、繼續在同樣的地方施展了《降雨》讓該地區陷入有如瀑布降下般暴雨狀態的魔術、使霹靂大蜈蚣的百足陷入泥沼般的狀態。

「《旋風・冷卻・吹雪》！！」

緊接從凱爾那兒吹來強烈的低氣壓、被稱為冰的初級魔術《白霧》、捲起冰凍的漩渦、將陷入泥沼的霹靂大蜈蚣的百足冰凍著。

「好、固定住了！」
「趁現在趕快打爆它！！」
「別忘了打斷它蟲腿削減機動性喲！！」

新米冒險者們用魔術與鐵矢、短劍與狼牙棒攻擊行動被封印的魔物。乍看之下霹靂大蜈蚣好像比較可憐、但對他們來說、是不得不這樣做、否則無法勝利。

只不過是一隻蟲、但正因為是隻蟲子。從目擊的情報可以推測、不管是體力、臂力、體格都是各種頑強、全部都是比自己更強的對手。因此更不應該傲慢自負、而是全力以赴、用上所有可用的手排、將其粉碎、將其絞殺。這是上位指導者們的教誨。

「別亂來喲！盡可能的用魔術削弱它！！如果可以就把它放倒！！」
「知道了！！不要一直命令我！！」

庫德和蕾亞老樣子的在鬥嘴、一邊將手中的藥水喝下保持魔力、一邊將《白霧》持續施展在的霹靂大蜈蚣身軀上。在離他們的不遠處、在霹靂大蜈蚣的頭部附近待機。

「《燃燒吧・就這樣全部・燃燒殆盡吧》！！」

同時說出口的詠唱與言語、從凱爾手中射出火球⋯⋯發射出的《火球魔術》吞噬了大百足的頭部。

「唦唦唦唦唦唦唦唦唦唦！！」

悲鳴、撞到石壁的頭部依然左右晃動努力掙扎著、然而凱爾仍毫不留情、毫無慈悲的連發火球。

黑色外殼在重量級的狼牙棒對著腦門一發火焰打擊也僅讓裂痕的硬度。近距離的物理攻擊效果不佳⋯⋯⋯⋯與其說不佳、不如說是自己的力量不足、在最初的一擊就已經體會到了。

只能使用對昆蟲不利的屬性火與冰來進行攻擊。如果是夏莉或者阿斯忒里歐的話、那堅硬的甲殼恐怕會變得比紙還要薄吧、但不巧、那些值得信賴的前輩們不在場。

冒險者的常識、別給對手反擊的能力、否則會大幅增加死亡風險、由於魔力用盡想要從背包裡面拿出藥劑得瞬間、他們意象不到的事情發生了。

「唦唦唦唦唦唦唦唦唦唦唦唦唦唦唦唦唦唦唦唦唦唦！！」
「嗚哇！？」
「噫呀！？」

霹靂大蜈蚣全身顫抖、劇烈震動、衝擊波從身軀全方位擴散。不可視的強力一擊、打擊在周圍的新米冒險者們身上。無法理解發生什麼事情的新米冒險者、因為衝擊摔得一屁股蛋疼。

「咕⋯⋯糟了、這傢伙脫離拘束了⋯⋯！」

脫離了石牆與冰凍的身軀、大百足用燒焦的複眼注視著這邊。為了盡快重新準備、立即站起身的冒險者等人。但對於他們、憤怒的大百足卻突然往不同的方向跑了。

「唦唦唦唦唦唦唦唦唦唦唦唦唦唦唦唦唦唦唦唦唦唦！！」

是剛才集中攻擊的成效吧。不知道有多少條腿被打斷、啪唦啪唦的跑走、亂撞亂跑的撞到了岩石和樹木。仔細一看、黑色的複眼多數早已碳化、觸角幾乎被燒毀。

「阿、多虧了剛剛攻擊頭部、總算得救了呢」
「但是啊、要怎樣阻止它呢？」

盲目的於憤怒中掙扎、折斷周圍的木樹、撞破岩石的霹靂大蜈蚣。經驗告訴自己、沒有牽制的近身作戰是非常嚴厲的、雖然想從遠距離攻擊、目前既沒有魔力也沒有足夠的戰力。在剛剛的衝擊波⋯⋯把藥劑得瓶子震碎了。

「總之、不如把這包包拿起來擰一擰、或許還有一點點？目標好像還是看不見的樣子」
「問題是該由和喝呢？」

能夠回復的魔力大概微乎其微的吧。恐怕最多就在使用一次魔術而已、那麼隊伍裡面最擅長使用魔術攻擊的────

「⋯⋯拜託了、凱爾。一發定勝負、如果這樣都無法解決、就沒有其他攻擊手段了」
「嗯⋯⋯⋯⋯嗯」

緊張的氣氛侵蝕著凱爾的身體。如果這次無法一發定勝負的話、本次的委託將以失敗告終。身為冒險者的信任度將會降低、確實有如凝聚隊伍未來命運的最終一擊。

與暴走的大百足附近保持距離、在朝著空瓶用力擰包包的蕾亞旁、凱爾緊張的按住自己顫抖的雙手調整呼吸。

此時、腦海中浮現的是自己視為目標、白色劍士的背影。光是站立著就有如鮮花般美麗的她、也一定會毫無畏懼的面對眼前那憤怒忘我狂暴的魔物吧。

那麼、自己也去面對吧。無論多麼困難的情況、只要能堅定不移、就能向前邁出一步。

───

從樹海返回邊境的歸途中、稍微帶有笑容的庫德說道。

「所以、幹嘛一直不耐煩的表情呢？」
「啊⋯⋯嗯、抱歉」

委託成功了。火焰的最終一擊完美的將大百足的頭部燃燒殆盡了、收到了樹海之都的感謝、一行人正在悠閒地返回到邊境的街道上、不過不知為何、勝利的主角、凱爾表情似乎不是那麼開心。

「總覺得啊、和大百足最後的攻防戰啊」
「不是挺順利的嗎。目標暴走筋疲力盡的時候送它的頭部一發！！不是預想那樣嗎」

最終、凱爾所採取的手段、是在對方因爆走而疲憊不堪的時候將頭部燃燒殆盡的安全牌。狂暴的霹靂大蜈蚣、橫衝直撞、去攻擊不穩定的目標相當然有難度、而且也難以接近。

無法用華麗的方式完成戰鬥、沒有令人驚豔的攻擊手段、凱爾只能做到的是與理想不符的現實來結束這委託。

凱爾選擇了在非緊急情況下有意義和安全可靠的選擇。對於許多冒險者而言、他的方法將是值得讚揚的事情、但只是以這樣的方式完成了委託、在凱爾心中的理想與現實的差異、他無法坦率的接受委託完成了。

「話說回來、果然俺們依然還是缺少能夠決定勝負的傢伙啊」
「就是呢⋯⋯結果、堅硬的對手就依靠魔術、沒有魔力也沒有藥水也是無奈呀。也不知道是誰的小刀一點都沒威力呢」
「某小不點的箭矢也差不多呀」
「『⋯⋯⋯⋯蛤！』」

妳一言我一語的。庫德朝著蕾亞的顏面送上了一發鷹爪功、而蕾亞則是用靴子送給了庫德一發斷筋踢、相互嘴砲與怒視。

「還不因為你的近戰能力太爛嗎！前幾天、短劍不是啪搭的就被折斷了嗎、後續都是靠凱爾的近戰的不是嗎！遜咖」
「妳才是吧、魔術爛到屌炸天！剛剛也不是這樣嗎、什麼《火球魔術》的連燒焦的痕跡都沒有好吧！廢柴」
「喂喂喂！？兩位可以停止嗎！」

凱爾回到了以往的狀態、去阻止兩人的爭吵。

「不管怎樣、不解決這個問題的話、可能會影響到升格審查的說、新的夥伴也會很難尋找呢」

冒險者公會會定期舉行升格的審查會。例如、Ｅ級要想升級為Ｄ級、就要應該要有與Ｄ級相稱的能力、完成委託的數量以及冒險者個人的人格。

連同與異性關聯、私生活的糾紛也會受到影響、相當意外而嚴格審查、不過、這些對於三人來說似乎沒有太大的問題。恐怕唯一的問題、就是隊伍能力的審查了吧。

像面對這次體能強度高的敵人、遇到了如此困難的麻煩、那麼就會在審查時留下相當不好的印象。輔助兩人、大男一人、不管怎樣都無法否認缺乏APAD的事實。

「真希望阿斯忒里歐桑在這裡呢」
「不再就沒辦法了」

在新註冊冒險者時指導他們的阿斯忒里歐早已離開了隊伍、正在指導今夏剛登錄的冒險者新人。因為、如果隊伍裡一直有高級冒險者參加、就會妨礙凱爾等人晉升。

「⋯⋯⋯⋯再說、也太熱了」
「⋯⋯⋯⋯樹海都是木陰才沒注意到、像這樣走在平原上就是煉獄了」
「⋯⋯嗚嗚、好想早點使用騎乘龍⋯⋯雖然賺不到每趟都能用的錢⋯⋯」

三人各自的煩惱、在這炙熱的陽光下幾乎無法思考。用乘著風奔馳於大地的騎乘龍出遠門一定很爽快吧、但很遺憾的是、三人都沒有到達可以駕馭騎乘龍的程度、再說想租借也沒錢⋯⋯。

「待會就休息一下吧、馬上就要到中途的森林了。那裡也有個湖」
「贊成～」

邊境的街道和樹海之間有一座小小的湖泊。因為離這裡近在咫尺、所以三人筋疲力盡的像湖泊方向走去、補充著消耗的水分。多虧當時聽信了夏莉的忠告、三人應該是不會中暑了。

「終、終於到了～」
「已經受不了、我要先跳水了！！」

宛如殭屍般的步伐走到樹林中的湖泊旁、看著那透明度極高的湖泊、水面上吹來的涼風、瞬間讓自己降溫了許多、可謂天堂。

游泳是夏天最大的娛樂活動。被日曬的三人、在這種天氣下說著『受不了、忍不住了』想就這樣穿著衣服跳進湖泊中、不過、緊接在之後凱爾發現奇怪的現象。

「等一下，你們兩個」
「哎！！幹嘛啦！？」
「我想早點跳下去解脫！！？」
「不是啦、看那個」

凱爾手指的方向是離湖中心很近的地方、深度絕對不是人類可以觸及深處。在中心處嘩啦嘩啦的濺起水花。

水中的生物在騷動、難道湖泊裡有魔物嗎、三人立即警戒著、仔細凝視一看、從劇烈波動的水面上、出現的是白皙的小手。

「⋯⋯⋯⋯該不會溺水了吧！？」
「真假！？」
「那得趕快救人呀！」

穿著衣服一起跳進了湖中、急忙游向湖中心。到了夏天、他們三個都是經常在泉水或者湖裡游泳的人、泳技自然相當出色、實在讓人難以相信他們身上都還穿著衣服正在吸水增加重量、凱爾更是拔得頭籌。

「沒問題⋯⋯嗎⋯⋯？」
「咳咳⋯⋯⋯⋯咳咳⋯⋯⋯⋯」

把庫德和蕾亞拋在一旁、讓溺水著的上半身浮出水面、此時凱爾注意到這個人有令人熟悉的長長白髮。

濕潤的頭髮、反射著陽光、黑色的泳裝彷彿在強調雪白的皮膚、並且直視在自己胳膊中女子臉龐、蒼與紅色的眼眸、凱爾認知到自己的直覺是正確的事。

「⋯⋯⋯⋯哎？凱爾、桑？為、為什麼會在這裡⋯⋯！？」

玲瓏貌美的夏莉、與自己所憧憬的人如此的近距離。柔軟纖細的肢體。而且、意識到胸前柔軟的感覺、豐滿的胸部靠著自己的胸膛時、凱爾瞬間失去了意識。

「哇啊啊啊啊！凱、凱爾！凱爾啊！！」
「為何臉色如此蒼白！？還有呼吸嗎！！？」

在那晶瑩剔透的湖面上、轉瞬之間染成了紅色、就這樣⋯⋯冒險者們的慘叫聲響徹了夏日的天空。


════════════════════

霹靂大蜈蚣
  RRRRRRRRRRR
泳裝與雙子充分出場的章節

９／１０ 三卷發售 漫畫於夏季放送