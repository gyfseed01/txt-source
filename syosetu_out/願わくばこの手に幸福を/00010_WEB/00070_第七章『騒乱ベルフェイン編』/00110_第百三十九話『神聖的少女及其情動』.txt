「恩，當然──選擇那種愚蠢的方法是不可能的。」

阿琉珥娜輕哼一聲。

她的話語無疑透露出一股慈愛，卻摻雜著像是在嘲笑對方似的扭曲的色彩。阿琉珥娜自己也不知道為什麼會從嘴裡會說出那樣糅雜著複雜感情的話語。
但是，儘管如此，阿琉珥娜還是看起來還很開心似地晃動著嘴唇。

「如果只是單純地取下罪人首級的話，就談不上懲罰了。沒錯，讓他活下去，活到最後，然後讓他明白自己的愚蠢，那才是他應該受到的懲罰。然後，在他將自己的罪行贖清之後，再給予他救贖。」

阿琉珥娜像是在念台詞般流暢地說著。

「我們的神，會給予罪人救贖──當然，也需要他們付出必要的代價。」

接待室裡奏出的那聲音仿彿是歌唱，美麗而又縈繞於耳的聲音。這就是阿琉珥娜被稱為歌姫的理由之一。
阿琉珥娜閉上嘴唇，眨了眨眼，將視線投向眼前的人──卡麗婭・帕多利克。

她的臉上浮現出端正的笑容，但那笑容中還夾帶著略微的怨言般的感情。這一點可以理解，畢竟我的說法和她的希望確實有所不同。嘛，確實會讓她的情緒變得糟糕一些吧。阿琉珥娜想著。

卡麗婭說，她和路易斯有仇，追趕著他的踪影，和芙拉朵・娜・波魯克庫拉特一起來到貝爾菲因。

誠然，實情如何姑且不論，至少，路易斯作為紋章教重要的一員攻陷了城市国家伽羅亞瑪利亞這件事被世人所知曉。那樣的話，當然會產生怨恨，執著地採取行動，復仇的願望也會萌發。那樣的事態，阿琉珥娜深深地理解著。

為了滿足這種怨恨和執著的感情，她們造訪了貝爾菲因，這並不是不可能的事。情緒有時會踐踏理智和常識，使人向前邁進。這也是事實。
正因如此，即使被告知寬恕罪行，給予救贖是大聖堂的教誨，也不能坦率地接受。

那我為什麼要對她們說這樣的話呢？
阿琉珥娜稍稍抿住嘴，明白自己的血正略微帶著熱量在體內循環著。原來如此，我好像對她們對路易斯抱有的感情感到很不愉快。

憎恨路易斯無所謂，擁有敵意也好，偏見也好，歧視也好，都無所謂。
不管怎麼說，牽起路易斯手的，只要有自己就足夠了。即使世界要拋棄他，只要由自己握住那隻手就好。所以，無論是誰憎恨著路易斯，都無所謂。

但是，儘管如此，我還是覺得很不愉快。
雖然身為上流階級，但卡麗婭和芙拉朵卻毫無芥蒂地一直追趕著路易斯，追尋到貝爾菲因來。阿琉珥娜對她們的執著很不滿意。

阿琉珥娜認為感情沒有正負可言，有的只是方向性和強弱這樣的基準。只是人們自作主張地給它們取了各種各樣的名字而已。
眼前的人，至少卡麗婭這名少女，毫無疑問地對路易斯抱有某種堅固的感情。在敘述路易斯的事情的時候，言語的節奏，銀瞳中浮現的光輝，非常引人注目。

對於阿琉珥娜來說，這實在是太令自己不爽了。敵意也好，憎恨也好，那種強烈的感情是不能容忍的。
阿琉珥娜理解了自己在不知不覺中變得非常貪婪的事實。無論是何種感情，我似乎完全無法接受他人對路易斯抱有強烈的感情。心中浮現出自嘲的笑容，真是麻煩的性格啊。

但是，現在絶不能把這種感情露骨地表現出來。因為現在自己是作為大聖堂的聖女候補在這裡的。雖然沒有風，但阿琉珥娜的金髮卻微微搖擺著。

「──握住伸出的手，給予救贖。這就是大聖堂應有的姿態。」

阿琉珥娜的臉頰不由自主地顫抖著，看著卡麗婭和芙拉朵兩人，再度張開嘴唇。阿琉珥娜認為說出那句話的自己一定非常醜陋。

「畢竟你們和路易斯只是陌生人。將執念堆積在心中，只會讓你們的罪孽變得深重。」

所以，安心地選擇遺忘不就好了嗎？表達出那樣的意思，黃金的瞳孔在室內閃耀著。

阿琉珥娜的言語，表情，姿態，毫無疑問就是聖女的姿態。從旁人看來，這無疑就是懷著慈愛之心，宣告著信仰的少女。
即使在其捲起情感漩渦的胸中，有一種無法稱之為神聖的感情，睜開了眼睛。


◇◆◇◆

──「那麼，路易斯，請道歉。請原諒我，就這麼對我說。」

那句話從嘴唇離開的瞬間，瑪蒂婭聽到了心臟劇烈的跳動，自己到底在說些什麼呢？這樣的動搖產生了，但是，現在的話肯定是自己的本心。兩種感情混雜在一起，心悸蔓延到全身。

瑪蒂婭的眼睛微微浮現出膽怯的顏色，窺視著路易斯。他會怎麼做呢？是驚訝地笑笑，還是投來蔑視的目光？一想到這些，瑪蒂婭的心臟就跳動的越發激烈。
我討厭那個。討厭被討厭。那樣的事，到現在為止想都沒想過。

誰會怎麼想？瑪蒂婭沒有閑暇為這種無聊的事而動搖感情。作為聖女，自己會被怎樣看待呢？應該有考慮過這種事吧，但是，瑪蒂婭作為個人，被如何看待，一次也沒有考慮過。那種想法，在瑪蒂婭身上是不存在的。身為聖女的瑪蒂婭，有個人的概念，只是在年幼的時候。

明明如此，現在自己卻在害怕被拒絶，簡直無法相信。牙齒咬合著，摩擦的聲音在嘴裡回響。如果說驚訝的話倒還好。但是被蔑視的話⋯可怕，討厭那樣。
路易斯像被瑪蒂婭的氣勢壓倒一樣退後一步，撫摸著自己的下巴。

陷入了一段時間的沉默，房間中的時間靜止了。在瑪蒂婭連呼吸都快要停止了的緊張中，他終於說話了。

「⋯⋯對不起。我知道那樣做不好。我會小心的，我發誓今後不再做那樣的事。」

路易斯看起來有點尷尬地說道。視線也完全偏離了瑪蒂婭，簡直就像是被老師訓斥的孩子一樣，瑪蒂婭的腦海中自然而然地浮現出這樣的想像。

瞳孔無意識地晃動著。

「⋯⋯不行啊。說了吧，要用心。好，再說一遍。」

瑪蒂婭的話語響起。路易斯舉起雙手，像是認輸了一樣，再次向我道歉。對，向我。
哦，那很好，路易斯，那就是能使你幸福的道路。
瑪蒂婭不由得地綳緊了臉頰。那種不檢點的表情，怎麼能讓人看到呢？更何況，對象還是路易斯。

但是，該怎麼阻止臉頰微微發燙呢？如何才能阻止內心深處的融化，阻止歡喜填滿內心呢？
現在，這樣就可以了。這樣也沒關係。他，路易斯一定只是以輕描淡寫的心情說出來的。但是，確實說出口了，順從了我。

──啊，這樣一來，每當他試圖投身於危險之中時，肯定都會想起我的臉。

雖然會覺得很郁悶，很麻煩，但還是會想起向我道歉的事情。每當他做危險的事情，我都會繼續做同樣的事情。為了能刻印在他的意識中。

沒錯，總有一天，當他要開始行動的時候，我這個存在就會成為腳鐐，束縛住他吧。
到最後，會變成總是窺視我的臉色，只能在我的管理下行動的路易斯。一定要那麼做。啊，那是多麼甜美的事情啊。只是想像一下，胸口就像要灼燒一樣地發燙。

瑪蒂婭的眼睛帶著熱度，閃耀著情感。在視野的邊緣，瑪蒂婭看見了布魯達打算張開嘴巴。

在那個瞬間。

──咚 咚

宣告著來訪者的聲音，響徹房間。