亞蓮跟巴布爾的比試結束之後不久。

劍武祭運營委員像捅了蜂巢似的騷亂起來。
這是因為，京城五學院之一──千刃學院的理事長蕾雅＝拉斯諾特突然來訪。

「您、您能駕臨這、這種地方的劍武祭⋯⋯！實、實在是誠惶誠恐⋯⋯！」

劍武祭的負責人、年已半百的男人一副十分動搖的樣子，竭力說著感謝的話語。
對此，莉亞一副有些抱歉的樣子搔了搔臉頰。

「那個⋯⋯不用特意給我準備VIP席，一般席就行了⋯⋯」

實際上這就是她的真心話。
她並不是來劍武祭視察的，只是作為一名觀眾來到這裡。
偶然被運營委員的人發現，正在驚訝的時候被帶到了VIP席。

「不不不！怎麼能那樣做！不如說我們只能給您準備這樣的席位，實在是萬分抱歉！」

這樣說著的男人一個勁地低頭道歉。
他之所以這麼惶恐也是有原因的。

五學院的理事長有著巨大的社會影響力和權力。
如果在這裡得罪了莉亞，連「劍武祭」也會非常簡單地就消失吧。
他身為劍武祭的負責人，像這樣誠惶誠恐也不是沒有道理的事。

「不用那麼拘謹。說是來看劍武祭，實際上我只是來看劍武祭的出場選手的。」

之後，莉亞為了消除運營委員的緊張講了點小笑話。
對她來說，也不想在這麼壓抑的氣氛下觀看比賽。

「最近為了千刃學院的進一步發展，我在各地奔波忙碌著。嘛啊，其實就是物色人才之類的事。」

雖然曾經的千刃學院極盡榮華，但近年來學生的質量在下降，五學院的地位也岌岌可危。
前任理事長引咎辭職後，在舉辦的理事長選舉中，莉亞成為了新任理事長。

她為了學院的復興在各地發掘優秀的劍士，如果確實有那份實力就向其提出免除學費的入學推薦。
一口氣改善學生質量是很難辦到的。所以，拉攏一部分超級天才，使得部分的質量得到提高這樣子。

「原來如此⋯⋯那、這次的目標果然是上一任的冠軍──『賞金獵人』嗎？」

稍稍平定緊張情緒的劍武祭負責人向莉亞問道。

「對，就是這樣。『賞金獵人蘿絲＝巴倫西亞』──早就想親眼見識一下了。」

如果蘿絲有著傳聞中的實力的話，莉亞打算立馬給她入學推薦。

「是這樣子的啊。我在上屆大會上第一次見到了賞金獵人的戰鬥⋯⋯那是一脈獨傳的櫻華一刀流！是陰氣逼人的可怕劍技啊！」
「那還真是值得期待。──話說，我也不是單單為了看蘿絲來到這裡的。」
「⋯⋯這話怎麼說？」
「或許能發掘出預想外的珍貴人材──期待著這麼一點點的可能性。」

這樣說著的莉亞，敏銳地洞察著舞台的動向。


■

在使用「一之太刀—飛影」將巴布爾打敗後，我勢如破竹的取得了連勝。
十戰十勝，就算是做夢也沒想到會取得這樣的成果。
等到注意到的時候，就到了下一場的冠軍決勝局了。

「已經做好決賽的準備了嗎？」

現場解說通過廣播宣布開始。

「觀眾們久等了！接下來上豔的是今天最後的對決！讓我們進入決賽！蘿絲＝巴倫西亞選手對上亞蓮＝羅德爾選手！有請兩位選手上台！」

賞金獵人蘿絲＝巴倫希亞。

我記得我聽過這名字，是與我同齡的天才女劍士。
他出沒於獎金比賽並漂亮的奪得冠軍。
也有發現並抓捕被懸賞的犯罪者，壓送給聖騎士這樣的事蹟。
不知道她到底有什麼目的，總之就是個技術高超的女劍士。

我和蘿絲登上了比武台。

她有著赤色的瞳孔，凜然的外表，略帶粉紅的美麗銀髮如瀑般散開。以黑色為基調的上衣用赤色的飾品點綴，從腹部到下胸大片大片地裸露著，讓人不知道把眼睛放哪裡好。下身穿著低腰熱褲。

她的戰鬥我一直在比武台的旁邊看著。
窈窕的身軀舞動的劍技，卻將精壯的漢子們玩弄於鼓掌，這只能用精彩來形容。

確認兩位參賽選手都上台後，現場解說像之前一樣開始了簡單的介紹。

「蘿絲＝巴倫西亞選手是一脈獨傳的秘劍—樓華一刀流的正統繼承人！另一邊的亞蓮選手則是自成一派～！」

解說說完後，大大地吸了一口氣，繼續說道。

「一但是，我可以斷言〜！把亞蓮選手自成一的劍術當成傻瓜的**，在場的各位裡一個都沒有！」

就像解說說的那樣，在我與巴布爾對戰後，嘲笑我的人 就沒有了。
如今不如說跟之前反過來了。

實際上，希望跟我握手的人蜂擁而至，甚至還有幾個人想當我的弟子。
握手倒也罷了，做我弟子還是算了吧。
我也只是個半吊子。
沒有教別人劍術的資格。

終於到了比賽開始的時候了，我像之前一樣點頭行禮。

「請賜教！」
「請～！」

蘿絲用如風鈴般清脆的聲音回覆道。
在我們打完招呼之後，解說宣布比試開始。

「雙方準備好了嗎？一那麼，決賽開始〜！」

話音剛落，我和要蘿絲就拔劍對準對方。
我們保持著完全相同的架勢，持續了─會兒對視。

（從至今為止的比試來看，她的戰鬥風格是反擊型）

防禦對手的攻擊的同時尋找破綻，抓住破綻從而一擊必殺的防守反擊之劍。
毫無對策地魯葬進攻是一招壊棋。（先用「飛影」攻擊，看看對方會怎麼應對會比較好嗎⋯⋯）

這樣這樣想著，我決定了接下來怎麼 出招，但在那個瞬間。

「！？」

螺絲的身體突然出現在咫尺之處。

（節奏被掌控了⋯⋯！？）

他在我吸氣眨眼的瞬間就突進到的我面前。
真是可怕至極的高速身法。

「櫻華一刀流—櫻閃〜！」她穩穩地降低重心，然後順著體重的反作用力突刺過來。
但是，就算被出其不意的攻擊擾亂了節奏，我也沒慌亂。

「──哈！」

面對她瞄準 我軀幹的突刺，我用跟她相對的入射方向的突刺進行迎擊。
然後，劍鋒與劍鋒勢均力敵地相互碰撞。
在鐵劍與鐵劍的猛烈衝撞下，爭爭聲響徹會場。

「騙 人的吧〜！？」

因想像不到的局勢而情不自禁地瞪大雙眼的蘿絲，在此刻露出了破綻。
我不會放過這個機會。
我立刻降低重心，利用重力加速度一口氣刺向她的胸口。

「一嘶拉〜！」
「！？」

我完美時機的突刺僅僅只是擦傷了蘿絲的側腹。
在這間不容髮的情形下，她強行扭轉身體躲開了我的突刺。
一定是因為她天賦異稟，又付出了常人難以想像的努力，才能夠躲開我的一擊吧。

「庫⋯⋯還沒完呢⋯！」

上一秒還在看側腹的蘿絲，下一秒就提劍砍了過來。

「櫻華一刀流──夜櫻〜！」

───

這之後我跟蘿絲大戰 了不知幾個回合。

在這期間，會場跟之前不同尋常地安靜。
歡呼聲，漫罵聲全都沒有。

在屏氣凝神地觀看我們的戰鬥的時候，偶爾也會小聲發出各種各樣的感想。

「喂喂，那個賞金獵人簡直是被當成孩子被玩弄⋯⋯」
「太牛Ｂ了⋯⋯果然我還是再去拜他為師吧⋯⋯」
「你傻啊，別給人家添麻煩。亞蓮大師可沒空管你這蠢材。」

在跟我的一次次交鋒中，蘿絲身上的傷痕越來越多。

「哈啊哈阿⋯⋯你這傢伙，這樣的劍術⋯到底是誰交給你的！？」
「之前不是說過了嗎⋯⋯我是自成一派來著⋯⋯」

自成一派果然不是什麼值得自豪的東西，不要再讓我說出口了啊。

「別騙人了！你的劍術是經過無數反覆試驗和鑽研的產物──有著歳月的痕跡啊！」

她用尖銳的眼神看向我，如此斷言道。

（好，好敏說⋯⋯）

確實，我的劍術是經過十多億年的歳月磨礪而成的，這大概就是蘿絲所感覺到的「歳月的痕跡」

「⋯⋯那，那是你的錯覺。」

我躲閃著眼神，這樣嘟噴道。
說實話，我一點也不想說關於一億年按鈕的話題。
就算說了，也沒人會相信吧。

「原來如此⋯⋯無論怎麼也不打算說出來嗎⋯⋯」

似乎是不滿意我的回答，蘿絲露出有些火大的表情。

「也罷，那就用你的身體來回答吧⋯⋯！一脈獨傳的秘劍，櫻華一刀流第十七任繼承人，在此，拿下這場勝負！」

話音才落，她身周的氣場驟變。比之前的氣勢更加銳利，好似她自身化成了一把吹毛斷發的神兵一般。

「櫻華一刀流奧義──鏡櫻斬～！」

宛如鏡像的左右各四擊──蘿絲使出了閃電般的八連擊。

（！？）

讓人聯想到櫻吹雪的華麗劍技，奪走了我一瞬間的注意力。
而在這瞬間八個斬擊猛然襲來。

但是──說到底那也只是「連擊」而已。
在一擊一擊的中間，仍然存在著微小的空隙。
將此看破的我，為迎擊蘿絲的奧義使出了劍技。

「八之太刀──八咫烏。」

這是只要揮一次就能斬八次的劍技。

雖然招式的名字裡都有「八」，但這井不是連擊一而是字面上的「同時」斬八次。
在一擊一擊的中間，不存在任何一點空隙，是真真正正的「一擊斬八劍」

就算是慢了一拍使出的八咫烏，依舊擊破了蘿絲的鏡櫻斬。

「怎麼，會⋯⋯！？」

必殺奧義被破的蘿絲，暴露出毫無防備的胴體。

「一結束了。」

對她破綻百出的身體，我給予一擊仁慈的袈裟斬。

「嘎，哈」

她雙膝一軟，緩緩向前倒下。
整個會場鴉雀無聲。

「勝，勝利者是──亞蓮＝羅德爾──！」

解說員宣布了比試的結果。

與此同時，會場也沸騰了起來。
用容易理解的言語表達的話就是──全都是對我的讚美。

就這樣，漂亮取得劍舞技優勝的我，拿到了足足有十萬戈爾德的大獎。


■

劍武祭之後過了兩天。

正當我像往常一樣一個人在操場練劍時，突然響起了校內廣播，內容是讓我去一趟校長辦公室。
總覺得有種不好的預感。

（⋯⋯不會是因為之前的謠言吧？）

我在與多多利艾爾的決鬥中使用了暗器，耍卑鄙手段才贏的──像這樣毫無根據的謠言。
在神聖的決鬥中，是禁止行使卑鄙手段的。
當然我不可能這麼做的，但學校會怎麼判定還不好說。

（哈啊～⋯⋯搞不好這次會被退學啊⋯⋯）

多多利艾爾背後的波頓男爵家有可能會向學校施壓逼我退學。

（也不對，我本來在學校裡就是吊車尾，因為成績慘不忍睹而被退學也不奇怪）

就算與多多利艾爾的決鬥無關，大概我也會被退學吧。這只是早晚的事。

（嘛啊，幸好我已經因一億年按鈕而有了一定的實力了）

有了能在劍武祭取勝的實力，也能夠進地方的騎士團了吧。
進了騎士團，當上聖騎士，就有了鐵飯碗。
這樣也能讓還在老家的我娘安度晚年了吧。

（嗯，也可以這麼做呢⋯⋯）

這樣想著，我來到了校長辦公室。
我敲了敲比起其他教室更顯莊嚴的門，報上了自己的名字。

「哦噢！亞蓮君、等你好久了！」

房門應聲打開，教導主任興高采烈地衝了出來。

（不止校長，教導主任也在⋯⋯）

這肯定沒錯。
今天我將不幸地離開這所學校了。

「快快，別在這站著，趕緊進來吧。」
「⋯⋯打擾了。」

跟著教導主任一進房間，校長也興高采烈地迎了過來。

「哦噢、亞蓮君！你來得正好！」
「您、您好～」

為什麼這兩人會這麼高興啊？
難道⋯⋯是因為終於可以剔除學校的吊車尾什麼的。
我心中大大地嘆了口氣。

「是這樣的，恭喜你收到入學推薦了！」

校長說出了我根本想不到的話。

「入學推薦⋯⋯？」
「對對對！是那所有數的名門──五學院的千刃學院。」

千刃學院──我聽過這個名字。那是無論哪個劍士都知道的京城豪門劍士學院之一。
劍術學院有初等部、中等部、高等部之分，這所格蘭劍士學院是供１３歳到１５歳學生上學的中等部。而千刃學院則是供１６歳到１８歳學生上學的高等部。

「像我們格蘭學院這樣窮鄉僻壤的地方去往五學院的升學者輩出──這可是了不得的豐功偉績啊！」
「到底為什麼亞蓮君會被千刃學院指定還搞不明白⋯⋯不管怎樣，搞錯了也無所謂！最重要的就是從我們這裡出現了學生升學五學院的事實！」

校長和教導主任始終一副興奮地樣子在說著。

「實在是、實在是幹得太好了！亞蓮君！」
「不愧是我們學校的學生，出色地盡到了你的本分！」
「哈、哈啊⋯⋯」

手和肩被兩人牢牢箍著的我只能生硬地回覆道。

「不，應該說不愧是亞蓮君啊！實際上我一開始就對你抱有很高的期待哦！」
「對了，你就作為畢業生代表去做答詞演講吧！當然，也是作為畢業生首席！」

之後兩個人就一直表揚我表揚我、總之就是一個勁兒地表揚我。
對於如此熱情的兩人，我抱以冷淡的目光。

（明明一直以來就徹底地無視著對我的欺凌⋯⋯來自千刃學院的入學推薦一出現就變成這副鬼樣，呵呵）

這兩人似乎無論如何都想要把我送進千刃學院。
對這個格蘭劍術學院來說，大概是像「鍍金」一樣的行為吧。

但是，是否接受這份推薦入學，不是我一個人能決定的。

「抱歉，那個、能讓我再考慮一下嗎？」

在我將自己的想法傳達出來的一瞬間。

「什麼？考慮？有什麼好考慮的！？」
「你是打算不接受這個入學推薦！？」

兩人神色大變，向我逼問道。

「是升學千刃學院還是去騎士團當聖騎士，又或者作為魔劍士去接受人們的委託，說實話，我還沒決定好。」

沒錯，這個選擇是將左右我今後的人生道路的大事。

「這事有必要跟還在老家的我娘⋯⋯娘親談談，現在沒法立刻決定下來。」

果然還是要回老家一趟，好好跟我娘商量商量才能決定下來吧。

「蠢、**！你想要白白浪費去千刃學院的機會嗎！？」
「只要從京城五學院畢業就能當上上級聖騎士！光輝的前途在等著你啊！」
「⋯⋯抱歉，到底怎麼選，現在還不知道。」

那之後，雖然兩人急紅了眼想要我選擇去千刃學院，但我卻完全不妥協。
到最後他們也堅持不住了，只得說出「靜候佳音」結束這個話題了。

「⋯⋯那、失禮了。」

在我離開校長辦公室後，不知是因為校長的傳呼還是聽到校內廣播，總之一大群老師圍了上來。

「亞蓮！我沒看錯，果然你是有才能的！亞蓮君？現在加入我的明神流怎麼樣？」
「不不不！他最適合我的真空流。怎麼樣、亞蓮君？我特意為你準備了準師範的位置哦！」
「你說什麼屁話！？像他這麼優秀的人，當然是適合風月流啊！」

看來我被千刃學院推薦入學的事，已經在老師之間傳開了。

三年前，在我還是一年級的時候，我向老師們拜託「請讓我加入您的流派」，卻被他們不留餘地地拒絶了。
平常他們對我在劍術上的提問也會露骨地露出厭煩的表情然後無視我⋯⋯現在卻這副鬼樣，呵呵。

（這就是常說的「見風使舵」吧⋯⋯）

當然，我完全不想加入這些不良老師的流派。
這些人大概就是想打出「從我的流派走出了升學到名門千刃學院的學生」這樣的宣傳噱頭，才會熱情洋溢地勸誘我加入吧。

「不好意思打擾了。」
「慢、慢著亞蓮君！至少把話聽完啊！」

我快速穿過了老師們，一個人離開了。

（哈啊⋯⋯總之這周末先回一趟老家，跟我娘商量一下）

話說，今天真的是見識了人的卑劣。

想要到像我娘和寶菈這樣冰清玉潔的人身邊待會兒，淨化淨化自己的心靈。

於是，我暫且先回宿舍去了。