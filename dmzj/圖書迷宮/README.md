# novel

- title: 圖書迷宮
- title_zh1:
- author: 十字静
- illust: しらび
- source: http://q.dmzj.com/2607/index.shtml
- cover: https://images-na.ssl-images-amazon.com/images/I/811uwZAYwJL.jpg
- publisher:
- date: 2019-01-04T10:40:26+08:00
- status:
- novel_status: 0x0101

## illusts


## publishers

- dmzj

## series

- name: 图书迷宫

## preface


```
★通過第十屆MF文庫J新人賞三次選拔，評價兩極化的問題作品！
★出版社破例出版，甫發售即立刻再版，份量十足、設定宏大的巨作！
★將讀者一同帶入書中世界，以第二人稱視角呈現截然不同的閱讀風貌！

你必須回想起來。
你必須找出隱藏在心理創傷深處的殺父仇人，取回被奪走的名譽及失去的魔法。
必須與身為吸血鬼真祖的少女──阿爾緹莉亞一起行動。
你為此造訪圖書館都市亞歷山卓，並踏入存在所有書籍的圖書迷宮。
你有一項極大的障礙──你的記憶只能維持八個小時。
可是，這有方法能夠克服，確實有方法。
請你奮力掙扎，為了身為一名人類。為了找回所有記憶──
第十屆MF文庫J輕小說新人賞，通過三次選拔的問題作品在此問世──

足掻いてください、あなたが人間足りうるために。

あなたは思い出さなければなりません。心的外傷の奥に潜む父の仇を探し出し、奪われた名誉と失った魔法を取り戻すのです。吸血鬼の真祖(ハイ・デイライトウォーカー)の少女、アルテリアと共に。そのために図書館都市を訪れ、ありとあらゆる本が存在する図書迷宮に足を踏み入れたのですから。あなたには一つの大きな障害があります。あなたの記憶は八時間しか保ちません。ですが、方法はあります。確かにあるのです。
足掻いてください、あなたが人間足りうるために。全ての記憶を取り返すために。
第十回MF文庫Jライトノベル新人賞、三次選考通過の問題作、ここに刊行―――。 
```

## tags

- node-novel
- dmzj
- 侦探
- 日本
- 魔法
- 第二人稱
- 奇幻
- 

# contribute

- 深夜读书会
- 读书群：714435342

# options

## dmzj

- novel_id: 2607

## downloadOptions

- noFilePadend: true
- filePrefixMode: 4
- startIndex: 1

## textlayout

- allow_lf2: true

# link

- https://www.youtube.com/watch?v=xel1-R9Am4Q
- https://twitter.com/10sedecim
- 
