# 檔案結構說明

> 其他介紹請看這裡 https://github.com/bluelovers/node-novel/tree/master/packages/layout-pattern/docs

## 設定檔

> 所有 .md 檔案 除了每本小說底下的 README.md (可以看的見小說設定/簡介/翻譯君/貢獻者名單的那個檔案) 以外
> 所有其他的 .md (包含 README.md) 都是自動生成的
> 不需要人工編輯

### README.md

> @require @root @subdir
> 小說的相關資訊  
> 經過 node-novel-info 解析後會變成[此格式](https://github.com/bluelovers/node-novel-info/blob/master/index.d.ts)

1. 包含原文生肉網址 以及 部分相關的連結
2. 如果為 成為小說家吧 的小說 則會透過 dip.jp 額外追加例如標籤之類的訊息
3. contribute 則是代表相關的整合 或者 翻譯 或者 各種 對此小說提出貢獻的名單
4. 以及部分腳本會讀取此檔案內的設定

* ~~部分早期整合的小說檔名可能會是 meta.md 或者 xxx.json (但於部分新的腳本內不支援~~

#### 備註

- 各小說底下的每個子目錄都能具有 簡易版 README.md 會從中讀取 封面 說明 等等資訊

### ATTACH.md

> @optional @subdir

> 紀錄小說各種附加檔案的資訊  
> (目前只支援圖片，可藉此支援內文內放置插圖)  
> 相關說明請看 https://github.com/bluelovers/node-novel-epub

範例請參考 [Genocide Online ～極惡千金的玩遊戲日記～ ATTACH](../girl/Genocide%20Online%20～極惡千金的玩遊戲日記～/00000_null/ATTACH.md)

### CONTRIBUTE.md

> @optional @subdir @root

當此檔案存在時 會在 epub 內加入此檔案內的內容  
並且可以

1. 自由設定標題 (`# 你所想要設定的標題`)
2. 支援部分 md 所不支援的 html 代碼 - 請看 [範例](https://github.com/bluelovers/node-novel-epub/blob/master/test/res/test/CONTRIBUTE.md) - 但請注意 此範例內所出現的東西並不是代表全部都能用
3. 支援部分特殊語法
4. 主目錄 與 子目錄都能放置此檔案

## 小說內容

### .txt

> 原始檔案皆以 .txt 格式來儲存

1. 支援少部分 html 語法並且可無視全形半形 請參考此檔案 [排版格式.txt](https://github.com/bluelovers/node-novel-epub/blob/master/test/res/test/0000/%E6%8E%92%E7%89%88%E6%A0%BC%E5%BC%8F.txt)
2. 支援於內文內放置插圖 請參考 ATTACH.md
3. 生成出來的 epub 會與 txt 的排版相同 不會產生多餘空行
4. 檔案名稱請參考 [檔名規則](檔名規則.md)
5. 其他請參考 [layout-pattern](https://github.com/bluelovers/node-novel/tree/master/packages/layout-pattern/docs)
6. 

## 自動生成

> 部分檔案雖然會生成但不會提交進 git 內

### 導航目錄.md

> @readonly @root

> 小說導航與一些無腦連結

1. 此檔案會跟隨每日自動生成 epub txt 時一起更新
2. 此檔案內的章節順序會與 生成後的 epub txt 完全相同
3. 

### 譯名對照.md

> @readonly @root

> @ChrisLiu 的處理腳本所輸出的譯名對照

### 待修正屏蔽字.md

> @readonly @root

> 偵測各種被貼吧屏蔽變成 `***` 的片段

### ja.md

> @readonly @root

> 偵測各章節是否包含日文(用來判斷是不是沒翻譯或者因圖片檔而用原文代替的章節)

### ja2.md

> @readonly @root

> 偵測目前小說內容下非漢字日文名並且會列出該名字所在的段落

### 英語.md

> @deprecated @readonly @root

> 偵測英文單字

* 此檔案因為沒有優化過濾僅適合 章節不多的時候能用而已
* 因為章節一多 此檔案就會變得閱讀性極差

### 待確認文字.md

> @deprecated @readonly @root

> 偵測段落是否正確或者部分容易錯誤的文字

* 此檔案因為沒有優化過濾僅適合 章節不多的時候能用而已
* 因為章節一多 此檔案就會變得閱讀性極差

### cover.xxx

> @local @root @subdir

> 封面圖片(製作epub時 也會嘗試讀取此檔案)

## footnote

- @deprecated 代表廢棄功能
- @readonly 代表自動生成 不可編輯 或者 編輯後會被覆蓋
- @local 代表在本地端才支援這個檔案
- @require 代表必須要有此檔案
- @optional 如果存在時則會有額外功能
- @subdir 此檔案放置於小說底下的子資料夾
- @root 此檔案放置於小說底下的根目錄
- 
